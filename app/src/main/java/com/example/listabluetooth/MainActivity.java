package com.example.listabluetooth;
import java.text.SimpleDateFormat;

import java.time.LocalDate;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.content.Intent;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class MainActivity extends AppCompatActivity {

    View view;
    private static final String TAG = "MainActivity";

    BluetoothAdapter mBluetoothAdapter;

    // Create a BroadcastReceiver for ACTION_FOUND
    private final BroadcastReceiver mBroadcastReceiver1 = new BroadcastReceiver() {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            // When discovery finds a device
            if (action.equals(mBluetoothAdapter.ACTION_STATE_CHANGED)) {

                final int state =  intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, mBluetoothAdapter.ERROR);

                switch(state) {
                    case BluetoothAdapter.STATE_OFF:
                        Log.d(TAG, "onReceive: STATE OFF");
                        break;
                    case BluetoothAdapter.STATE_TURNING_OFF:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING OFF");
                        break;
                    case BluetoothAdapter.STATE_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE ON");
                        break;
                    case BluetoothAdapter.STATE_TURNING_ON:
                        Log.d(TAG, "mBroadcastReceiver1: STATE TURNING ON");
                        break;
                }
            }
        }
    };

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy: called.");
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnONOFF = (Button) findViewById(R.id.buttonONOFF);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        btnONOFF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: enabling disabling bluetooth.");
                enableDisableBT(view);
            }
        });

    }


    public void enableDisableBT( View v){
        boolean tmp = false;
        Button btnONOFF = (Button) findViewById(R.id.buttonONOFF);

        if(mBluetoothAdapter == null){
            Log.d(TAG, "enableDisableBT: Does not have BT capabilities.");
        }
        if(!mBluetoothAdapter.isEnabled()){
            Log.d(TAG, "enableDisableBT: enabling BT.");

            Intent enableBTIntent2 = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            startActivity(enableBTIntent2);

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);



            while (tmp == false) {

                if (findAttendance().contains("04:B1:67:3A:BC:8D")) {
                    Log.d(TAG, "enableDisableBT: SI ENTRA POR ALGUNA RAZON");
                    tmp = true;
                    Animation anim = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.fade_in);
                    v.setBackgroundColor(tmp ? Color.GREEN : Color.RED);
                    btnONOFF.startAnimation(anim);
                    btnONOFF.setText("Si asististe a clase! ");


                }

            }

        }
        if(mBluetoothAdapter.isEnabled()){
            Log.d(TAG, "enableDisableBT: disabling BT.");
            mBluetoothAdapter.disable();

            IntentFilter BTIntent = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            registerReceiver(mBroadcastReceiver1, BTIntent);
            Animation anim = AnimationUtils.loadAnimation(v.getContext(), android.R.anim.fade_out);
            v.setBackgroundColor(Color.rgb(214,215,215));
            btnONOFF.startAnimation(anim);
            btnONOFF.setText("APAGADO ");
        }

    }


    public static String findAttendance()  {


        StringBuilder result = new StringBuilder();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        try
        {

            LocalDate now = LocalDate.now();
            LocalDate earlier = now.minusMonths(1);
            DateTimeFormatter  formatter = DateTimeFormatter.ofPattern("d-M");



            String fec= formatter.format(earlier);


            Log.d(TAG, "findAttendance: "+ earlier);
            URL url = new URL("https://firestore.googleapis.com/v1beta1/projects/attendancelistapp/databases/(default)/documents/attendance/"+fec+"-2019/students/201517667");

            Log.d(TAG, "findAttendance: URL" + url);
            try{
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line;
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            rd.close();
            return result.toString();}
            catch(IOException e)

            {
                Log.d(TAG, "findAttendance: io exception");

            }
        }
        catch (MalformedURLException e)
        {
            Log.d(TAG, "findAttendance: exception");
        }


        return "no";
    }

}


